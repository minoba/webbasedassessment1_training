<%--
  Created by IntelliJ IDEA.
  User: michiel
  Date: 11/01/2018
  Time: 10:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Assignment1Result</title>
</head>
<body>
<h2>Sequence created</h2>
<h3>Length = ${param.seq_length}</h3>
<h3>Type = ${param.seq_type}</h3>

<p>
    ${requestScope.sequence}
</p>

</body>
</html>
