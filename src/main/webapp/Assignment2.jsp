<%--
  Created by IntelliJ IDEA.
  User: michiel
  Date: 11/01/2018
  Time: 15:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Assignment2</title>
</head>
<body>

<h1>Hello, ${requestScope.name}</h1>

<jsp:include page="Assignment2Include.jsp" >
    <jsp:param name="role" value="${requestScope.role}" />
</jsp:include>

</body>
</html>
