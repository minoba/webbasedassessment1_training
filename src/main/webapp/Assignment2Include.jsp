<%--
  Created by IntelliJ IDEA.
  User: michiel
  Date: 11/01/2018
  Time: 15:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:choose>
    <c:when test="${param.role == 'GUEST'}">
        <h2>Hello dear guest, please have a look at our content</h2>
    </c:when>
    <c:otherwise>
        <h2>Howdy admin, want to do some maintenance?</h2>
    </c:otherwise>
</c:choose>
