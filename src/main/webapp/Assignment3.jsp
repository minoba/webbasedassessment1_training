<%--
  Created by IntelliJ IDEA.
  User: michiel
  Date: 15/01/2018
  Time: 20:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Assignment 3</title>
</head>
<body>

<ol>
    <c:forEach var="port" items="${sessionScope.server_ports}">
        <li>Port ${port.port} is in use by ${port.name}</li>
    </c:forEach>
</ol>
</body>
</html>
