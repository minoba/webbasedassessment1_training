package nl.bioinf.wis;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Creation date: 10-1-2018
 *
 * @author Michiel Noback (&copy; 2018)
 * @version 0.01
 */
@WebServlet(name = "Assignment1Servlet", urlPatterns = "/give.dna")
public class Assignment1Servlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String sequenceType = request.getParameter("seq_type");
        String lengthStr = request.getParameter("seq_length");
        int length = Integer.parseInt(lengthStr);
        int maxLength = DataProvider.DNA.length();

        if (length > maxLength) {
            length = maxLength;
        }

        String returnSequence = DataProvider.DNA.substring(0, length);
        if (sequenceType.equals("RNA")) {
            returnSequence = returnSequence.replaceAll("T", "U");
        }

        request.setAttribute("sequence", returnSequence);

        RequestDispatcher view = request.getRequestDispatcher("Assignment1Result.jsp");
        view.forward(request, response);

    }
}
