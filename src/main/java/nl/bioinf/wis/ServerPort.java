package nl.bioinf.wis;

/**
 * Creation date: Jan 15, 2018
 *
 * @author Michiel Noback (&copy; 2018)
 * @version 0.01
 */
public class ServerPort {
    private String name;
    private String port;

    public ServerPort(String name, String port) {
        this.name = name;
        this.port = port;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }
}
