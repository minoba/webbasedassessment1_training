package nl.bioinf.wis;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "Assignment2Servlet", urlPatterns = "/greeting.do")
public class Assignment2Servlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = DataProvider.NAMES[(int)(Math.random() * DataProvider.NAMES.length)];
        String role = DataProvider.ROLES[(int)(Math.random() * DataProvider.ROLES.length)];

        request.setAttribute("name", name);
        request.setAttribute("role", role);

        RequestDispatcher view = request.getRequestDispatcher("Assignment2.jsp");
        view.forward(request, response);
    }
}
