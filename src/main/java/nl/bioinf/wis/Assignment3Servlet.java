package nl.bioinf.wis;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "Assignment3Servlet", urlPatterns = "/list.ports")
public class Assignment3Servlet extends HttpServlet {

    private List<ServerPort> ports;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        session.setAttribute("server_ports", ports);
        RequestDispatcher view = request.getRequestDispatcher("Assignment3.jsp");
        view.forward(request, response);

    }

    @Override
    public void init() throws ServletException {
        String serverPorts = getServletContext().getInitParameter("server-ports");
        if (serverPorts == null) return;
        this.ports = new ArrayList<>();
        //System.out.println("serverPorts = " + serverPorts);
        //20:FTP,22:SSH,80:HTTP,443:HTTPS,995:POP3
        String[] elements = serverPorts.split(",");
        for (String element : elements) {
            //System.out.println("element = " + element);
            String[] portElements = element.split(":");
            ServerPort port = new ServerPort(portElements[1], portElements[0]);
            ports.add(port);
        }
    }
}
