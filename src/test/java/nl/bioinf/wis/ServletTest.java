package nl.bioinf.wis;

import org.junit.Test;
import org.mockito.Mockito;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.junit.Assert.assertEquals;

/**
 * Creation date: 10-1-2018
 *
 * @author Michiel Noback (&copy; 2018)
 * @version 0.01
 */
public class ServletTest  extends Mockito {
    @Test
    public void doPost() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);

        when(request.getParameter("type")).thenReturn("RNA");
        when(request.getParameter("length")).thenReturn("10");

        new Assignment1Servlet().doPost(request, response);

        //verify the parameter was accessed
        verify(request, atLeast(1)).getParameter("type");
        verify(request, atLeast(1)).getParameter("length");

        assertEquals(DataProvider.DNA.substring(0, 10), request.getAttribute("sequence"));
    }

    @Test
    public void doGet() throws Exception {
    }

}