## Test header (tentamen voorblad)

- **Teacher** Michiel Noback (NOMI), to be reached at +31 50 595 4691
- **Test size** 4 programming assignments
- **Aiding materials** Computer on the BIN network
- **Supplementary materials**  
    - `docs/html5-cheatsheet.jpg` HTML5 cheat sheet: the main html tags
    - `docs/JSP_cheat_sheet.pdf` JSP cheat sheet
    - `docs/corejava.pdf` Java cheat sheet by RefCardz
    
- **TO BE SUBMITTED  
This project, as `zip` archive. Use this name for the zip:
 `AppdesignAssessment<YOURNAME>-<YOURSTUDENTNUMBER>.zip`**

## Instructions

For this test, you should be logged in as guest (username = "gast", password = "gast"). 
On your Desktop you will find the assessment as a Gradle-managed Java project,
 as well as the submit script `submit_your_work`. 
 Open this project in IntelliJ Idea and deal with the assignments. Answer any textual questions
 in the provided `question_answers.properties` file that is located within the `answers` folder. 
After finishing, create a zip according to name given above and submit it using the submit script
 `submit_your_work`.
Type `submit_your_work --help` in the terminal to get help on its usage.

The possible number of points to be scored are indicated for all assignments. Your grade 
will be calculated as  
`Grade = (PointsScored/PossiblePoints * 10) < 1 ? 1 : (PointsScored/PossiblePoints * 10)`

**This assessment is (partially) graded automatically. You need to be extra careful to use correct spelling and 
follow the instructions exactly!**

## The Assignments

This project has a standard Gradle-managed layout for a Java web project. Some source files and resources have been provided.
It is up to you to implement the assignments in this project. Do not worry about styling - that is not being tested here.


#### Assignment 1: Form creation and handling request data (30 points)

Create an HTML page called `assignment1_form.html` with page title 'Assignment 1' and a decent header in it.
 In it, create a simple web form that a user can use to request a Nucleic Acid sequence. 
This form should include three elements. The first element should be a pull-down menu offering 
the choice of sequence type, DNA or RNA, with a default value of DNA. The second element should be a text field 
indicating the requested length of the sequence. The third element should be a submit button for the form.
Upon submitting, a request should be sent to the url `/give.dna`, with the two request parameters from the form
having names `seq_type` and `seq_length`. Handling this request will be dealt with in the next question.

Now create a servlet with the name `Assignment1Servlet`. It should be configured to receive requests originating from the form 
of `assignment1_form.html`. Implement the functionality associated with the web form: create a sequence of requested 
type and length. Class DataProvider already has a DNA sequence for your convenience (`DataProvider.DNA`);
you should use this sequence as base. When longer sequences are requested, simply adjust that value accordingly. 
When created, the sequence should be forwarded to a JSP page called `Assignment1Result.jsp` and shown within that page.
Also display the type and length of the created sequence. 


#### Assignment 2: Using includes and conditionals (30 points)

Create a new JSP called `Assignment2.jsp`. Make sure the page has this directive at the top:

```jsp
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
```

Create a second JSP, called `Assignment2Include.jsp`. This will be an included JSP (and also needs the same directive!).

Finally, create a Servlet, called `Assignment2Servlet` that serves the url `/greeting.do`.

**Now implement the following use case:**

When the url `/greeting.do` is requested, select a random name and a random role from the arrays provided
 by class dataprovider: 

```java
public class DataProvider {
    public static final String[] NAMES = {"Henk", "Jelle", "Anita"};
    public static final String[] ROLES = {"ADMIN", "GUEST"};
    //more data
}
```

Next, pass the randomly selected name and role variables to the JSP `Assignment2.jsp`.
In this JSP, show the `name` at the top of the page. Include `Assignment2Include.jsp` in this page and  
pass it the `role` variable as parameter.
 
Finally, in the include, make a conditional selection of content to show: If the role is "ADMIN", you should 
display the sentence *"Howdy admin, want to do some maintenance?"*, but if the role is "GUEST", 
you should display the sentence *"Hello dear guest, please have a look at our content"*.

**TIP:** Random selection of elements from an array can be performed like this:

```java
String name = DataProvider.NAMES[(int)(Math.random() * DataProvider.NAMES.length)];
```

#### Assignment 3: Application- and session-scoped data and JSP foreach looping (30 points)

File `webapp/WEB-INF/web.xml` has the following content:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_3_1.xsd"
         version="3.1">
    <context-param>
        <param-name>server-ports</param-name>
        <param-value>20:FTP,22:SSH,80:HTTP,443:HTTPS,995:POP3</param-value>
    </context-param>
</web-app>
``` 

Create a Servlet called `Assignment3Servlet` that serves the url `/list.ports`. In this servlet,
read in the data from `web.xml`, parse it into a list of `ServerPort` instances (create this regular Java
class yourself), and attach this list as a session-scoped variable called "server_ports" before forwarding
the view to `Assignment3.jsp`. You also need to create this JSP of course. 
In this JSP, list all server ports (port + name) as an ordered list.


#### Assignment 4: Javascript event handling (10 points)

This assignment should be implemented using the html file `Assignment4.html`.
It already has this content:

```html
<!DOCTYPE html>
<html lang="en">
    <meta charset="UTF-8">
    <title>Assignment 4</title>
    <link rel="stylesheet" type="text/css" href="style.css">

    <script src="jquery-3.2.1.min.js"></script>
    <script src="site.js"></script>
</head>
<body>
<h1>Javascript demo</h1>

<button id="clickme">Click me!</button>

<button id="hidden" class = "hidden">I used to be hidden</button>
</body>
</html>
```

The associated stylesheet looks like this:

```css
.hidden {
    visibility: hidden;
}

.visible {
    visibility: visible;
}
``` 

And the stub for javascript is this:

```js
window.onload = function () {
    //DO YOUR THING
}
```

It is your task to make the button that is hidden become visible when the "Click me!" button is clicked,
and make it invisible again when it is clicked again and so forth - thus toggling the visibility.
You may use jQuery, but pure Javascript is also OK.

That's it.